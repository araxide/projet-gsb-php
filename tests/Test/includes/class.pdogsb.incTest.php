<?php

/**
 * Classe de tests pour les manipulations avec la BDD
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    Emeline RAIMOND <emelineraimond@gmail.com>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */

 require_once 'C:\wamp\www\GSB_AppliMVC\includes\class.pdogsb.inc.php';

class PdoGsbTest extends PHPUnit\Framework\TestCase {

    /**
     * @var PdoGsb
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new PdoGsb;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers PdoGsb::__destruct
     * @todo   Implement test__destruct().
     */
    public function test__destruct() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getPdoGsb
     * @todo   Implement testGetPdoGsb().
     */
    public function testGetPdoGsb() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getVisiteurs
     * @todo   Implement testGetVisiteurs().
     */
    public function testGetVisiteurs() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getInfosVisiteur
     * @todo   Implement testGetInfosVisiteur().
     */
    public function testGetInfosVisiteur() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getInfosComptable
     * @todo   Implement testGetInfosComptable().
     */
    public function testGetInfosComptable() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getLesFraisHorsForfait
     * @todo   Implement testGetLesFraisHorsForfait().
     */
    public function testGetLesFraisHorsForfait() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getNbjustificatifs
     * @todo   Implement testGetNbjustificatifs().
     */
    public function testGetNbjustificatifs() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getLesFraisForfait
     * @todo   Implement testGetLesFraisForfait().
     */
    public function testGetLesFraisForfait() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getLesIdFrais
     * @todo   Implement testGetLesIdFrais().
     */
    public function testGetLesIdFrais() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::majFraisForfait
     * @todo   Implement testMajFraisForfait().
     */
    public function testMajFraisForfait() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::majFraisHorsForfait
     * @todo   Implement testMajFraisHorsForfait().
     */
    public function testMajFraisHorsForfait() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::majNbJustificatifs
     * @todo   Implement testMajNbJustificatifs().
     */
    public function testMajNbJustificatifs() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::modifieStatutRefuse
     * @todo   Implement testModifieStatutRefuse().
     */
    public function testModifieStatutRefuse() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::estPremierFraisMois
     * @todo   Implement testEstPremierFraisMois().
     */
    public function testEstPremierFraisMois() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::dernierMoisSaisi
     * @todo   Implement testDernierMoisSaisi().
     */
    public function testDernierMoisSaisi() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::creeNouvellesLignesFrais
     * @todo   Implement testCreeNouvellesLignesFrais().
     */
    public function testCreeNouvellesLignesFrais() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::creeNouveauFraisHorsForfait
     * @todo   Implement testCreeNouveauFraisHorsForfait().
     */
    public function testCreeNouveauFraisHorsForfait() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::supprimerFraisHorsForfait
     * @todo   Implement testSupprimerFraisHorsForfait().
     */
    public function testSupprimerFraisHorsForfait() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getLesMoisDisponibles
     * @todo   Implement testGetLesMoisDisponibles().
     */
    public function testGetLesMoisDisponibles() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getLesInfosFicheFrais
     * @todo   Implement testGetLesInfosFicheFrais().
     */
    public function testGetLesInfosFicheFrais() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::majEtatFicheFrais
     * @todo   Implement testMajEtatFicheFrais().
     */
    public function testMajEtatFicheFrais() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::majMontantFicheFrais
     * @todo   Implement testMajMontantFicheFrais().
     */
    public function testMajMontantFicheFrais() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getLibelleFraisHF
     * @todo   Implement testGetLibelleFraisHF().
     */
    public function testGetLibelleFraisHF() {
        // Remove the following lines when you implement this test.
        // $this->markTestIncomplete(
                // 'This test has not been implemented yet.'
        // );
        $pdoInstance = new PdoGsb();
        $libelle = $pdoInstance->getLibelleFraisHF(21, 'a131', 201609);
        $this->assertEquals($libelle, 'Taxi');
    }

    /**
     * @covers PdoGsb::getMontantFraisHF
     * @todo   Implement testGetMontantFraisHF().
     */
    public function testGetMontantFraisHF() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getTotalMontantFraisHF
     * @todo   Implement testGetTotalMontantFraisHF().
     */
    public function testGetTotalMontantFraisHF() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getTotalMontantFraisForfait
     * @todo   Implement testGetTotalMontantFraisForfait().
     */
    public function testGetTotalMontantFraisForfait() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers PdoGsb::getLesFicheFraisAPayer
     * @todo   Implement testGetLesFicheFraisAPayer().
     */
    public function testGetLesFicheFraisAPayer() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

}
