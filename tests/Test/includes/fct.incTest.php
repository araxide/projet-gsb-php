<?php

/**
 * Classe d'exemple de tests pour les fonctions de l'appli GSB
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    Emeline RAIMOND <emelineraimond@gmail.com>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
require_once 'C:\wamp\www\GSB_AppliMVC\includes\fct.inc.php';

use PHPUnit\Framework\TestCase;

class FctIncTest extends TestCase {

    /**
     * @covers dateFrancaisVersAnglais
     */
    public function testDateFrancaisVersAnglais() {
        // if (dateFrancaisVersAnglais('16/02/2019') === '2019-02-16') {
        // echo 'test dateFrancaisVersAnglais : ok';
        // } else {
        // echo 'test dateFrancaisVersAnglais : erreur';
        //}
        $date = dateFrancaisVersAnglais('16/02/2019');
        echo'$date';
        $this->assertEquals($date, '2019-02-16');
    }

}
