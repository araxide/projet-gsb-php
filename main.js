/**
 * Fonctions JavaScript pour l'application Gsb
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    Emeline Raimond <emelineraimond@gmail.com>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */

/*
 * Enregistre les variables dans un objet JSON pour les restiter
 * 
 * @param String id du formulaire sur lequel se base la soumission
 * 
 * @return false pour empêcher la soumission du formulaire
 */
function corrigerFrais(id)
{
    $(id).on('submit', function () {
        var that = $(this), // this représente l'objet c'est à dire le formulaire 
                url = that.attr('action'), // on récupère l'action du formulaire
                type = that.attr('method'), // on récupère la méthode du formulaire -POST
                data = {};

        // maintenant on va itérer à travers les éléments du formulaire, ceux qui ont un name
        that.find('[name]').each(function (index, value) {
            var that = $(this),
                    name = that.attr('name'),
                    value = that.val();

            data[name] = value; // on a récupéré tous les éléménts dans un objet 
        });

        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function (response) {
                console.log(response);
            }
        });
        return false; // pour ne pas que le formulaire se soumette 
    });
}

/**
 * Teste si la liste des mois pour un visiteur est vide
 * 
 * @return null
 */
function estVideMoisVisiteurs()
{
    if (!$('#lstMoisVisiteurs').val()) {
        alert('Pas de mois disponible pour ce visiteur!');
    }
}

/**
 * Teste si la liste des fiches est vide
 * 
 * @return null
 */
function estVideLstFiches()
{
    if (!$('#lstFiches').val()) {
        alert('Pas de fiches à suivre pour le moment!');
    }
}

/**
 * Teste si la liste des fiches est vide, pour empêcher la soumission du formulaire
 * 
 * @return false pour empêcher la soumission du formulaire
 */
function verifieSiValeur()
{
    if (!$('#lstFiches').val()) {
        alert('Pas de fiches à suivre pour le moment!');
        return false;
    }
}

